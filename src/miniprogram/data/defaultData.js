const defaultTime = 1512840830;
//生成默认分类
const getDefaultCategory = () => {
  const categoryArry = [];
  const color = ["#4CB050", "#2196F3", "#9C28B1", "#F44236", "#08B8D0", "#F49100", "#735144", "#5C7581", "#F74481", "#424242", "#009688", "#3F51B5"];
  const name = ["重要", "一般", "论坛", "金融"];

  for (let i = 0; i < color.length; i++) {
    const category = {
      "color": color[i],
      "id": i + 1,
      "name": i < name.length ? name[i] : '分类' + (i + 1),
      "sort": i + 1,
      "time": defaultTime
    };
    categoryArry.push(category);
  }
  return categoryArry;
}
//生成默认Key数据
const getDefaultKey = () => {
  const categoryArry = getDefaultCategory();
  const keyArry = [];
  let index = 0;

  for (let i = 0; i < 5; i++) {
    for (let j = 0; j < 3; j++) {
      index++;
      const category = categoryArry[i];

      const key = {
        "categoryColor": category.color,
        "categoryID": category.id,
        "categoryName": category.name,
        "categorySort": category.sort,
        "keyAccount": index + "xxxxx@qq.com",
        "keyID": index,
        "keyName": "示例" + index,
        "keyPwd": "123456",
        "keyPwd2": "654321",
        "keyRemark": "备注" + index,
        "keyUrl": "http://www.qq.com",
        "sort": index,
        "time": defaultTime
      }
      keyArry.push(key);
    }
  }
  return keyArry;
};
//生成默认userInfo
const getDefaultUserInfo = () => {
  return {
    openID: "",
    unionID: "",
    enPassword: "",
    isCloudSyn: false,
    nickName: "",
    avatarUrl: "",
    gender: "",
    province: "",
    city: "",
    country: "",
    language: "",
    time: defaultTime
  }
}
//默认数据
const getDefaultJsonData = () => {
  return {
    version: 1,
    time: defaultTime,
    userInfo: getDefaultUserInfo(),
    category: getDefaultCategory(),
    key: getDefaultKey()
  }
}

//暴露接口
module.exports = {
  getDefaultCategory: getDefaultCategory,
  getDefaultKey: getDefaultKey,
  getDefaultUserInfo: getDefaultUserInfo,

  getDefaultJsonData: getDefaultJsonData
}